# Description:
Parser is an application that can parse json and csv files into json file.

# Technologies used:
1) Spring core <br />
2) OpenCsv <br />
3) Jackson-databind <br />
4) Javax-validation <br />
5) Lombok <br />

# Getting Started Guide:
1) Download the application. <br />
2) Generate .jar file by running maven clean install <br />
3) Run java -jar jar_file.jar absolute_csv_or_json_path absolute_csv_or_json_path <br />

# Input files example:
1) Json:<br />
{"orderId":8,"amount":3996,"currency":"EUR","comment":"оплата заказа"} <br />

2) Csv: <br />
-1,0,USD,оплата заказа <br />

Output: <br />
{"id":8,"orderId"=8,"amount"=3996.0,"comment"="оплата заказа","filename"="/home/input.json","line"="8","result"="OK"} <br />
{"id":-1,"orderId"=-1,"amount"=0.0,"comment"="оплата заказа","filename"="/home/input.csv","line"="1","result"="amount should be positive, id -1 should be positive"} <br />
