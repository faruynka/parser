package com.my.parser.service;

import com.my.parser.domain.Order;
import com.my.parser.parser.Parser;
import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class OrderReaderImpl implements Runnable {
    private final String filePath;
    private final Parser parser;
    private final BlockingQueue<Order> orders;
    private final AtomicBoolean readerStopped;

    public OrderReaderImpl(String filePath, BlockingQueue<Order> orders, Parser parser, AtomicBoolean readerStopped) {
        this.filePath = filePath;
        this.parser = parser;
        this.orders = orders;
        this.readerStopped = readerStopped;
    }

    public void read() throws IOException, InterruptedException {
        try (FileReader filereader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader(filereader)) {
            String line;
            long linePosition = 0;
            while ((line = bufferedReader.readLine()) != null) {
                linePosition += 1;
                synchronized (orders) {
                    while (orders.remainingCapacity() == 0) {
                        orders.wait();
                    }
                    Order order = parser.parse(line, filePath, linePosition);
                    if (order == null) {
                        continue;
                    }
                    orders.add(order);
                    orders.notify();
                }
            }
        }
        synchronized (orders) {
            readerStopped.set(true);
            orders.notify();
        }
    }

    @SneakyThrows
    @Override
    public void run() {
        read();
    }
}
