package com.my.parser.service;

import com.my.parser.domain.Order;
import com.my.parser.parser.Parser;
import com.my.parser.parser.impl.CsvParser;
import com.my.parser.parser.impl.JsonParser;
import org.apache.commons.io.FilenameUtils;
import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class ReaderFactory {
    private Map<String, Parser> parserMap;

    public ReaderFactory(ApplicationContext context) {
        parserMap = new HashMap<String, Parser>() {
            {
                put("csv", context.getBean(CsvParser.class));
                put("json", context.getBean(JsonParser.class));
            }
        };
    }

    public Runnable createReader(String path, BlockingQueue<Order> orders, List<AtomicBoolean> stoppers) {
        String extension = FilenameUtils.getExtension(path).toLowerCase();
        Parser parser = parserMap.get(extension);
        if (parser == null) {
            throw new IllegalArgumentException(extension + " extension is illegal");
        }
        AtomicBoolean stopper = new AtomicBoolean();
        stoppers.add(stopper);
        return new OrderReaderImpl(path, orders, parser, stopper);
    }
}
