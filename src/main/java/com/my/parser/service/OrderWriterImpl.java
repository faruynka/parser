package com.my.parser.service;

import com.my.parser.domain.Order;
import lombok.SneakyThrows;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class OrderWriterImpl implements Runnable {
    private final BlockingQueue<Order> orders;
    private final List<AtomicBoolean> stoppers;

    public OrderWriterImpl(BlockingQueue<Order> orders, List<AtomicBoolean> stoppers) {
        this.orders = orders;
        this.stoppers = stoppers;
    }

    public void write() throws InterruptedException {
        Order order;
        while (true) {
            synchronized (orders) {
                while (!checkAllStopped() && orders.isEmpty()) {
                    orders.wait();
                }
                if (checkAllStopped() && orders.isEmpty()) {
                    break;
                }
                order = orders.poll();
                System.out.println(order);
                orders.notify();
            }
        }
    }

    private Boolean checkAllStopped() {
        for (AtomicBoolean stopper : stoppers) {
            if (!stopper.get()) {
                return false;
            }
        }
        return true;
    }

    @SneakyThrows
    @Override
    public void run() {
        write();
    }
}
