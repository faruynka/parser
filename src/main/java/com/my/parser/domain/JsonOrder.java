package com.my.parser.domain;

import com.my.parser.validator.constraints.Id;
import com.my.parser.validator.constraints.ValidBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

@AllArgsConstructor
@Data
@Validated
@ValidBean("jsonOrder")
public class JsonOrder {
    @Id
    private Long orderId;

    @NotNull(message = "amount has invalid value")
    @Positive(message = "amount should be positive")
    private Double amount;

    @NotBlank(message = "currency has invalid value")
    private String currency;

    @NotBlank(message = "comment has invalid value")
    private String comment;

    public JsonOrder() {
    }
}
