package com.my.parser.domain;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {

    private final List<InvalidField> invalidFields = new ArrayList<>();

    public void addInvalidFields(List<InvalidField> invalidFields) {
        this.invalidFields.addAll(invalidFields);
    }

    public List<InvalidField> getInvalidFields() {
        return new ArrayList<>(invalidFields);
    }
}
