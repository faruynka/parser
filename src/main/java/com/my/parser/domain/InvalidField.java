package com.my.parser.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class InvalidField {
    private String name;
    private Object value;
    private String errorMessage;

    public InvalidField() {
    }

    public InvalidField(String name, String value, String errorMessage) {
        this.name = name;
        this.value = value;
        this.errorMessage = errorMessage;
    }
}
