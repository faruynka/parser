package com.my.parser.domain;

import com.my.parser.validator.constraints.Id;
import com.my.parser.validator.constraints.ValidBean;
import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@AllArgsConstructor
@Data
@ValidBean("csvOrder")
public class CsvOrder {
    @Id
    @CsvBindByPosition(position = 0, required = true)
    private Long id;

    @NotNull(message = "amount has invalid value")
    @Positive(message = "amount should be positive")
    @CsvBindByPosition(position = 1, required = true)
    private Double amount;

    @NotBlank(message = "currency has invalid value")
    @CsvBindByPosition(position = 2, required = true)
    private String currency;

    @NotBlank(message = "comment has invalid value")
    @CsvBindByPosition(position = 3, required = true)
    private String comment;

    public CsvOrder() {
    }
}
