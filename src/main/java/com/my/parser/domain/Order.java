package com.my.parser.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
@Data
public class Order {
    private Long id;

    private Long orderId;

    private Double amount;

    private String comment;

    private String fileName;

    private Long line;

    private String result;

    public Order() {
    }

    public Order(String fileName, Long line, String result) {
        this.fileName = fileName;
        this.line = line;
        this.result = result;
    }

    @Override
    public String toString() {
        return "{\"id\":" + id +
                ",\"orderId\"=" + orderId +
                ",\"amount\"=" + amount +
                ",\"comment\"=\"" + comment + '"' +
                ",\"filename\"=\"" + fileName + '"' +
                ",\"line\"=\"" + line + '"' +
                ",\"result\"=\"" + result + "\"}";
    }
}
