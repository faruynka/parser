package com.my.parser;

import com.my.parser.domain.Order;
import com.my.parser.service.OrderWriterImpl;
import com.my.parser.service.ReaderFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class ParserApplication {
    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext("com.my.parser");
        if (args.length != 2) {
            System.out.println("Failed to start application. Two file paths have to be passed as parameters.");
            return;
        }
        BlockingQueue<Order> orders = new ArrayBlockingQueue<>(10, true);
        List<AtomicBoolean> stopperList = new ArrayList<>();
        ReaderFactory readerFactory = new ReaderFactory(context);
        List<Thread> readers = new ArrayList<>();
        for (String arg: args) {
            readers.add(new Thread(readerFactory.createReader(arg, orders, stopperList)));
        }
        Thread writerThread = new Thread(new OrderWriterImpl(orders, stopperList));
        for (Thread reader: readers) {
            reader.start();
        }
        writerThread.start();
        try {
            for (Thread reader: readers) {
                reader.join();
            }
            writerThread.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
