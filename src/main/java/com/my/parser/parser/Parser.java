package com.my.parser.parser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.my.parser.domain.Order;
import com.my.parser.transformer.Transformer;
import com.my.parser.validator.OrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class Parser<T> {
    private static final Integer RECORD_SIZE = 4;
    private static final String RECORD_SIZE_ERROR_MESSAGE = "Record size has to be " + RECORD_SIZE;
    private static final String MESSAGE_STATUS_OK = "OK";

    private final Transformer<T> transformer;

    @Autowired
    private OrderValidator<T> validator;

    public Parser(Transformer<T> transformer) {
        this.transformer = transformer;
    }

    public Order parse(String strToParse, String fileName, Long linePosition) {
        if (strToParse.isEmpty()) {
            return null;
        }
        String statusMessage = "";
        if (strToParse.split(",").length != RECORD_SIZE) {
            statusMessage = RECORD_SIZE_ERROR_MESSAGE;
        }
        T order = null;
        try {
            order = parseStrToOrder(strToParse);
        } catch (Exception e) {
            String errorMessage = "Cannot parse string '" + strToParse + "'";
            statusMessage = modifyStatusMessage(statusMessage, errorMessage);
            return transformer.transform(order, fileName, linePosition, statusMessage);
        }
        String validationMessage = validator.validate(order);
        if (!validationMessage.isEmpty()) {
            statusMessage = modifyStatusMessage(statusMessage, validationMessage);
        }
        if (statusMessage.isEmpty()) {
            statusMessage = MESSAGE_STATUS_OK;
        }
        return transformer.transform(order, fileName, linePosition, statusMessage);
    }

    public abstract T parseStrToOrder(String strToParse) throws JsonProcessingException;

    private String modifyStatusMessage(String statusMessage, String errorMessage) {
        return statusMessage.isEmpty() ? errorMessage :
                statusMessage.concat("; " + errorMessage);
    }

}
