package com.my.parser.parser.impl;

import com.my.parser.domain.CsvOrder;
import com.my.parser.parser.Parser;
import com.my.parser.transformer.Transformer;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.StringReader;

@Component
public class CsvParser extends Parser<CsvOrder> {
    public CsvParser(@Qualifier("csvToOrder") Transformer<CsvOrder> transformer) {
        super(transformer);
    }

    @Override
    public CsvOrder parseStrToOrder(String strToParse) {
        return (CsvOrder) new CsvToBeanBuilder(new StringReader(strToParse))
                .withType(CsvOrder.class)
                .withIgnoreEmptyLine(true)
                .withThrowExceptions(false)
                .build()
                .parse().get(0);
    }
}
