package com.my.parser.parser.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.my.parser.domain.JsonOrder;
import com.my.parser.parser.Parser;
import com.my.parser.transformer.Transformer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class JsonParser extends Parser<JsonOrder> {
    public JsonParser(@Qualifier("jsonToOrder") Transformer<JsonOrder> transformer) {
        super(transformer);
    }

    @Override
    public JsonOrder parseStrToOrder(String strToParse) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(strToParse, JsonOrder.class);
    }
}
