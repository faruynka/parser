package com.my.parser.transformer.impl;

import com.my.parser.domain.CsvOrder;
import com.my.parser.domain.Order;
import com.my.parser.transformer.Transformer;
import org.springframework.stereotype.Component;

@Component("csvToOrder")
public class CsvToOrder implements Transformer<CsvOrder> {
    @Override
    public Order transform(CsvOrder csvOrder, String fileName, Long linePosition, String statusMessage) {
        if (csvOrder == null) {
            return new Order(fileName, linePosition, statusMessage);
        }
        return new Order(csvOrder.getId(), csvOrder.getId(), csvOrder.getAmount(), csvOrder.getComment(),
                fileName, linePosition, statusMessage);
    }
}
