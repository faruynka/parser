package com.my.parser.transformer.impl;

import com.my.parser.domain.JsonOrder;
import com.my.parser.domain.Order;
import com.my.parser.transformer.Transformer;
import org.springframework.stereotype.Component;

@Component("jsonToOrder")
public class JsonToOrder implements Transformer<JsonOrder> {
    @Override
    public Order transform(JsonOrder jsonOrder, String fileName, Long linePosition, String statusMessage) {
        if (jsonOrder == null) {
            return new Order(fileName, linePosition, statusMessage);
        }
        return new Order(jsonOrder.getOrderId(), jsonOrder.getOrderId(), jsonOrder.getAmount(), jsonOrder.getComment(),
                fileName, linePosition, statusMessage);
    }
}
