package com.my.parser.transformer;

import com.my.parser.domain.Order;

public interface Transformer<G> {
    Order transform(G order, String fileName, Long linePosition, String statusMessage);
}
