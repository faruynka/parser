package com.my.parser.validator;

public interface OrderValidator<T> {
    String validate(T order);
}
