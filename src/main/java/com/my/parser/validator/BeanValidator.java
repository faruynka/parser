package com.my.parser.validator;

import com.my.parser.domain.ValidationResult;

public interface BeanValidator {
    ValidationResult validate(Object bean);
}
