package com.my.parser.validator.impl;

import com.my.parser.domain.InvalidField;
import com.my.parser.exception.ValidationException;
import com.my.parser.validator.FieldValidator;

import java.lang.reflect.Field;
import java.text.MessageFormat;

public class IdValidator implements FieldValidator {
    @Override
    public InvalidField validate(Object object, Field field) {
        long id = 0;
        try {
            id = (Long) field.get(object);
            if (id < 0) {
                return new InvalidField(field.getName(), id, MessageFormat.format("{0} {1} should be positive",
                        field.getName(),id));
            }
        } catch (NumberFormatException | NullPointerException e) {
            return new InvalidField(field.getName(), id, MessageFormat.format("id {0} has invalid value", id));
        } catch (IllegalAccessException e) {
            throw new ValidationException(e.getMessage());
        }
        return null;
    }
}
