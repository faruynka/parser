package com.my.parser.validator.impl;

import com.my.parser.domain.InvalidField;
import com.my.parser.domain.ValidationResult;
import com.my.parser.validator.BeanValidator;
import com.my.parser.validator.FieldValidator;
import com.my.parser.validator.constraints.*;
import com.my.parser.validator.OrderValidator;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OrderValidatorImpl<T> implements OrderValidator<T> {
    private final Validator validator;
    Map<Class<? extends Annotation>, FieldValidator> validatorMap = new HashMap<>();
    private BeanValidator beanValidator;

    public OrderValidatorImpl() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        validatorMap.put(Id.class, new IdValidator());
        beanValidator = new AnnotationFieldValidator(validatorMap);
    }

    @Override
    public String validate(T order) {
        String errorMessage = "";
        for (ConstraintViolation<T> violation : validator.validate(order)) {
            errorMessage = modifyErrorMessage(errorMessage, violation.getMessage());
        }
        ValidationResult result = beanValidator.validate(order);
        List<InvalidField> invalidFields = result.getInvalidFields();
        for (InvalidField invalidField : invalidFields) {
            if (invalidField.getErrorMessage() != null) {
                errorMessage = modifyErrorMessage(errorMessage, invalidField.getErrorMessage());
            }
        }
        return errorMessage;
    }

    private String modifyErrorMessage(String errorMessage, String validationResult) {
        if (errorMessage.isEmpty()) {
            return validationResult;
        }
        return errorMessage.concat(", " + validationResult);
    }
}
