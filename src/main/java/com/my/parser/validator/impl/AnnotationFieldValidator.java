package com.my.parser.validator.impl;

import com.my.parser.domain.ValidationResult;
import com.my.parser.exception.ValidationException;
import com.my.parser.validator.BeanValidator;
import com.my.parser.validator.FieldValidator;
import com.my.parser.validator.constraints.ValidBean;
import com.my.parser.domain.InvalidField;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class AnnotationFieldValidator implements BeanValidator {
    private final Map<Class<? extends Annotation>, FieldValidator> validators;
    private final Set<Class<? extends Annotation>> supportedAnnotations;

    public AnnotationFieldValidator(Map<Class<? extends Annotation>, FieldValidator> validators) {
        this.validators = validators;
        supportedAnnotations = validators.keySet();
    }

    public ValidationResult validate(Object object) {
        ValidationResult validationResult = new ValidationResult();
        if (object == null) {
            throw new ValidationException("Cannot validate null object");
        }
        Class<?> clazz = object.getClass();
        if (!clazz.isAnnotationPresent(ValidBean.class)) {
            String errorMessage = MessageFormat.format("{0} does not have the {1} annotation.",
                    clazz, ValidBean.class.getName());
            throw new ValidationException(errorMessage);
        }
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            List<InvalidField> invalidFields = supportedAnnotations.stream()
                    .filter(field::isAnnotationPresent)
                    .map(validators::get)
                    .map(fieldValidator -> fieldValidator.validate(object, field))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            validationResult.addInvalidFields(invalidFields);
        }
        return validationResult;
    }
}


