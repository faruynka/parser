package com.my.parser.validator;

import com.my.parser.domain.InvalidField;

import java.lang.reflect.Field;

public interface FieldValidator {
    InvalidField validate(Object object, Field field);
}
